###callculates the empirical covariance
emp_cov <- function(X, Y){
  x_mean <-  mean(X)
  y_mean <-  mean(Y)
  return ( 1 / length(X) * sum( ( X - x_mean) * ( Y - y_mean)))
  }