### Creates a vector with the multiplicative inverse of each element or zero if the element is zero
lambda_plus <- function(x) {
  if (!any(is.na(x))) {
  return(ifelse(x == 0, 0, x ^ (-1)))
  }
  return(NA)
}

#### Creates the inverse Moore Penrose Matrix
moore_penrose <- function(A) {
  B <- eigen(A)
  return(B[["vectors"]] %*% diag(lambda_plus(B[["values"]])) %*% t(B[["vectors"]]))
}
